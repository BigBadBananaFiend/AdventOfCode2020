# Advent of Code day 1 – Part 1

with open('C:\\Users\\Dalibor\\Desktop\\Projects\\AdventOfCode2020\\entries.txt') as f:
    entries_str = f.readlines()
    entries = [int(i) for i in entries_str]

    for i in entries:
        a = 2020 - i
        if a in entries:
            print(a*i)
            break

# Advent of Code day 1 – Part 2

    for i in entries:
        a = 2020 - i
        possible_matches = [j for j in entries if j != a]
        for j in possible_matches:
            b = 2020 - i - j
            if b in entries:
                print(i * j * b)
                break

# Advent of Code day 2 – Part 1

with open('C:\\Users\\Dalibor\\Desktop\\Projects\\AdventOfCode2020\\pass.txt') as m:
    pass_entries = m.readlines()

    result = 0

    for i in pass_entries:
        temp = i.split()
        x, y, z, r = temp[0].split('-')[0], temp[0].split('-')[1], temp[1].replace(':', ''), temp[2]
        if r.count(z) in range(int(x), int(y) + 1):
            result += 1

    print(result)

# Advent of Code day 2 – Part 2

with open('C:\\Users\\Dalibor\\Desktop\\Projects\\AdventOfCode2020\\pass.txt') as m:
    pass_entries = m.readlines()

    result = 0

    for i in pass_entries:
        temp = i.split()
        x, y, z, r = temp[0].split('-')[0], temp[0].split('-')[1], temp[1].replace(':', ''), temp[2]
        if r[int(x) - 1] == z and r[int(y) - 1] != z:
            result += 1
        elif r[int(x) - 1] != z and r[int(y) - 1] == z:
            result += 1

    print(result)

# Advent of Code day 3 – Part 1


def slope_checker(xTimes, yTimes):

    with open('C:\\Users\\Dalibor\\Desktop\\Projects\\AdventOfCode2020\\path.txt') as m:
        fields = [l.strip() for l in m.readlines()]

        x = 0
        y = 0
        z = len(fields)
        c = 0

        while y < z - 1:
            x += xTimes
            y += yTimes
            l = fields[y]
            if l[x % len(l)] == "#":
                c += 1

    return c


def slopes_checker():
    one = slope_checker(1, 1)
    two = slope_checker(3, 1)
    three = slope_checker(5, 1)
    four = slope_checker(7, 1)
    five = slope_checker(1, 2)

    result = one * two * three * four * five

    print(result)

slopes_checker()








